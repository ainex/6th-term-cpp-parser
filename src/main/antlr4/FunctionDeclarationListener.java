// Generated from D:/PROG/JAVA/repositories/cpp-parser/src/main/edu/ulyanova/nstu/sixthterm\FunctionDeclaration.g4 by ANTLR 4.7
package antlr4;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link FunctionDeclarationParser}.
 */
public interface FunctionDeclarationListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link FunctionDeclarationParser#functionDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDeclaration(FunctionDeclarationParser.FunctionDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionDeclarationParser#functionDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDeclaration(FunctionDeclarationParser.FunctionDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionDeclarationParser#declarationSpecifier}.
	 * @param ctx the parse tree
	 */
	void enterDeclarationSpecifier(FunctionDeclarationParser.DeclarationSpecifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionDeclarationParser#declarationSpecifier}.
	 * @param ctx the parse tree
	 */
	void exitDeclarationSpecifier(FunctionDeclarationParser.DeclarationSpecifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionDeclarationParser#parameterTypeList}.
	 * @param ctx the parse tree
	 */
	void enterParameterTypeList(FunctionDeclarationParser.ParameterTypeListContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionDeclarationParser#parameterTypeList}.
	 * @param ctx the parse tree
	 */
	void exitParameterTypeList(FunctionDeclarationParser.ParameterTypeListContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionDeclarationParser#parameterList}.
	 * @param ctx the parse tree
	 */
	void enterParameterList(FunctionDeclarationParser.ParameterListContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionDeclarationParser#parameterList}.
	 * @param ctx the parse tree
	 */
	void exitParameterList(FunctionDeclarationParser.ParameterListContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionDeclarationParser#parameterDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterParameterDeclaration(FunctionDeclarationParser.ParameterDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionDeclarationParser#parameterDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitParameterDeclaration(FunctionDeclarationParser.ParameterDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionDeclarationParser#abstractDeclarator}.
	 * @param ctx the parse tree
	 */
	void enterAbstractDeclarator(FunctionDeclarationParser.AbstractDeclaratorContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionDeclarationParser#abstractDeclarator}.
	 * @param ctx the parse tree
	 */
	void exitAbstractDeclarator(FunctionDeclarationParser.AbstractDeclaratorContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionDeclarationParser#arrayParamDeclarator}.
	 * @param ctx the parse tree
	 */
	void enterArrayParamDeclarator(FunctionDeclarationParser.ArrayParamDeclaratorContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionDeclarationParser#arrayParamDeclarator}.
	 * @param ctx the parse tree
	 */
	void exitArrayParamDeclarator(FunctionDeclarationParser.ArrayParamDeclaratorContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionDeclarationParser#storageClassSpecifier}.
	 * @param ctx the parse tree
	 */
	void enterStorageClassSpecifier(FunctionDeclarationParser.StorageClassSpecifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionDeclarationParser#storageClassSpecifier}.
	 * @param ctx the parse tree
	 */
	void exitStorageClassSpecifier(FunctionDeclarationParser.StorageClassSpecifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionDeclarationParser#typeSpecifier}.
	 * @param ctx the parse tree
	 */
	void enterTypeSpecifier(FunctionDeclarationParser.TypeSpecifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionDeclarationParser#typeSpecifier}.
	 * @param ctx the parse tree
	 */
	void exitTypeSpecifier(FunctionDeclarationParser.TypeSpecifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionDeclarationParser#typeQualifier}.
	 * @param ctx the parse tree
	 */
	void enterTypeQualifier(FunctionDeclarationParser.TypeQualifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionDeclarationParser#typeQualifier}.
	 * @param ctx the parse tree
	 */
	void exitTypeQualifier(FunctionDeclarationParser.TypeQualifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionDeclarationParser#functionSpecifier}.
	 * @param ctx the parse tree
	 */
	void enterFunctionSpecifier(FunctionDeclarationParser.FunctionSpecifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionDeclarationParser#functionSpecifier}.
	 * @param ctx the parse tree
	 */
	void exitFunctionSpecifier(FunctionDeclarationParser.FunctionSpecifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionDeclarationParser#typedefName}.
	 * @param ctx the parse tree
	 */
	void enterTypedefName(FunctionDeclarationParser.TypedefNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionDeclarationParser#typedefName}.
	 * @param ctx the parse tree
	 */
	void exitTypedefName(FunctionDeclarationParser.TypedefNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionDeclarationParser#pointer}.
	 * @param ctx the parse tree
	 */
	void enterPointer(FunctionDeclarationParser.PointerContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionDeclarationParser#pointer}.
	 * @param ctx the parse tree
	 */
	void exitPointer(FunctionDeclarationParser.PointerContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionDeclarationParser#typeQualifierList}.
	 * @param ctx the parse tree
	 */
	void enterTypeQualifierList(FunctionDeclarationParser.TypeQualifierListContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionDeclarationParser#typeQualifierList}.
	 * @param ctx the parse tree
	 */
	void exitTypeQualifierList(FunctionDeclarationParser.TypeQualifierListContext ctx);
}