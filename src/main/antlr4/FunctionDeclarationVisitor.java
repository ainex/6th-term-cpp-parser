// Generated from D:/PROG/JAVA/repositories/cpp-parser/src/main/edu/ulyanova/nstu/sixthterm\FunctionDeclaration.g4 by ANTLR 4.7
package antlr4;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link FunctionDeclarationParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface FunctionDeclarationVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link FunctionDeclarationParser#functionDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionDeclaration(FunctionDeclarationParser.FunctionDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionDeclarationParser#declarationSpecifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclarationSpecifier(FunctionDeclarationParser.DeclarationSpecifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionDeclarationParser#parameterTypeList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameterTypeList(FunctionDeclarationParser.ParameterTypeListContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionDeclarationParser#parameterList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameterList(FunctionDeclarationParser.ParameterListContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionDeclarationParser#parameterDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameterDeclaration(FunctionDeclarationParser.ParameterDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionDeclarationParser#abstractDeclarator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAbstractDeclarator(FunctionDeclarationParser.AbstractDeclaratorContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionDeclarationParser#arrayParamDeclarator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayParamDeclarator(FunctionDeclarationParser.ArrayParamDeclaratorContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionDeclarationParser#storageClassSpecifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStorageClassSpecifier(FunctionDeclarationParser.StorageClassSpecifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionDeclarationParser#typeSpecifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeSpecifier(FunctionDeclarationParser.TypeSpecifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionDeclarationParser#typeQualifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeQualifier(FunctionDeclarationParser.TypeQualifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionDeclarationParser#functionSpecifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionSpecifier(FunctionDeclarationParser.FunctionSpecifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionDeclarationParser#typedefName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypedefName(FunctionDeclarationParser.TypedefNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionDeclarationParser#pointer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPointer(FunctionDeclarationParser.PointerContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionDeclarationParser#typeQualifierList}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeQualifierList(FunctionDeclarationParser.TypeQualifierListContext ctx);
}