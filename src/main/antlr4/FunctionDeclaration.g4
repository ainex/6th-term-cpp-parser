grammar FunctionDeclaration;

functionDeclaration
    :   declarationSpecifier+  Identifier '(' parameterTypeList? ')' ';'
    ;

declarationSpecifier
    :   storageClassSpecifier
    |   typeSpecifier
    |   typeQualifier
    |   functionSpecifier
    ;

parameterTypeList
    :   parameterList
    |   parameterList ',' '...'
    ;

parameterList
    :   parameterDeclaration
    |   parameterList ',' parameterDeclaration
    ;

parameterDeclaration
    :   declarationSpecifier+ abstractDeclarator?
    ;

abstractDeclarator
    :   pointer
    |   pointer? arrayParamDeclarator
    ;

arrayParamDeclarator
    :   '[' ']'
    |   arrayParamDeclarator '[' ']'
    ;

storageClassSpecifier
    :   'typedef'
    |   'extern'
    |   'static'
    |   'auto'
    |   'register'
    ;

typeSpecifier
    :   ('void'
    |   'char'
    |   'short'
    |   'int'
    |   'long'
    |   'float'
    |   'double'
    |   'signed'
    |   'unsigned')
    |   typedefName
    |   typeSpecifier pointer
    ;

typeQualifier
    :   'const'
    |   'restrict'
    |   'volatile'
    ;

functionSpecifier
    :   ('inline'
    |   '_Noreturn'
    |   '__stdcall')
    ;

typedefName
    :   Identifier
    ;

pointer
    :   '*' typeQualifierList?
    |   '*' typeQualifierList? pointer
    ;

typeQualifierList
    :   typeQualifier
    |   typeQualifierList typeQualifier
    ;

Identifier
    :   IdentifierNondigit
        (   IdentifierNondigit
        |   Digit
        )*
    ;

fragment
IdentifierNondigit
    :   Nondigit
    ;

fragment
Nondigit
    :   [a-zA-Z_]
    ;

fragment
Digit
    :   [0-9]
    ;

Whitespace
    :   [ \t]+
        -> skip
    ;

Newline
    :   (   '\r' '\n'?
        |   '\n'
        )
        -> skip
    ;