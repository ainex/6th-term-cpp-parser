package edu.ulyanova.nstu.sixthterm;

import edu.ulyanova.nstu.sixthterm.services.ParseResult;
import edu.ulyanova.nstu.sixthterm.services.ParseService;
import edu.ulyanova.nstu.sixthterm.services.ParseServiceImpl;
import org.antlr.v4.gui.TreeViewer;
import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Main form with Menu actions
 */
public class MainForm {
	private static final Logger LOGGER = Logger.getLogger(MainForm.class.getName());
	private static final int WIDTH = 1000;
	private static final int HEIGHT = 700;
	private static final String TITLE = "Simple Cpp function declaration parser";
	private static final String MENU = "Menu";

	//Menu items actions
	private static final String CREATE = "Create";
	private static final String OPEN = "Open";
	private static final String SAVE = "Save";
	private static final String SAVE_AS = "Save as ...";
	private static final String ENTER_THE_EXPRESSION_MESSAGE = "Expression is empty. A function prototype expression is expected.";
	private static final String PARSING_ERROR_MESSAGE = "Errors while parsing expression:";

	//Menu actions collection
	private final Map<String, Consumer<ActionEvent>> MENU_ACTIONS = new HashMap<>();

	private final ParseService parseService = new ParseServiceImpl();

	private JFrame jFrame;
	private JPanel panelMain;
	private JTextArea textArea;
	private JButton parseButton;
	private JPanel treePanel;
	private JLabel infoLabel;
	private JTextArea textAreaInfo;

	private JMenu menu;
	private JMenuBar menuBar;

	public MainForm() {
		this.jFrame = new JFrame(TITLE);
		this.menuBar = new JMenuBar();
		this.menu = new JMenu(MENU);
		createMenuItems();
		this.menuBar.add(menu);
		jFrame.setJMenuBar(menuBar);
	}

	public void init() {
		jFrame.setContentPane(this.panelMain);
		jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jFrame.pack();
		jFrame.setLocationRelativeTo(null);
		jFrame.setVisible(true);

		parseButton.addActionListener(new ParseButtonListener());
	}

	private void createMenuItems() {
		ItemListener itemListener = new ItemListener();
		Stream.of(CREATE, OPEN, SAVE, SAVE_AS).forEach(action -> {
			JMenuItem item = new JMenuItem(action);
			item.addActionListener(itemListener);
			this.menu.add(item);
		});

		initMenuActions();
	}

	private class ItemListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			LOGGER.info(e.toString());
			MENU_ACTIONS.get(e.getActionCommand()).accept(e);
		}
	}

	private class ParseButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			LOGGER.info("actionPerformed: " + e);
			treePanel.removeAll();
			treePanel.revalidate();
			treePanel.repaint();

			if (StringUtils.isBlank(textArea.getText())) {
				infoLabel.setText(ENTER_THE_EXPRESSION_MESSAGE);
			} else {
				ParseResult result = parseService.parseToTree(textArea.getText());

				if (null == result.getContext().exception) {
					TreeViewer treeViewer = result.getTreeViewer();
					treePanel.add(treeViewer);
					infoLabel.setText("");
				} else {
					infoLabel.setText(PARSING_ERROR_MESSAGE);
					treePanel.add(textAreaInfo);
					textAreaInfo.setText(result.getErrorListener().getErrorMessages());
					textAreaInfo.repaint();
				}
				treePanel.revalidate();
				treePanel.repaint();
			}
			textArea.requestFocus();
		}
	}

	private void initMenuActions() {
		MENU_ACTIONS.put(OPEN, actionEvent -> {
			LOGGER.info("action OPEN");
			JFileChooser fileChooser = new JFileChooser();
			if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				LOGGER.info("selected file: " + file.getName());
				try {
					String content = new String(Files.readAllBytes(file.toPath()));
					textArea.setText(content);
				} catch (IOException e) {
					logError(e);
				}
			}
		});

		MENU_ACTIONS.put(CREATE, actionEvent -> {
			LOGGER.info("action CREATE");
			textArea.setText("");
		});

		MENU_ACTIONS.put(SAVE, actionEvent -> {
			LOGGER.info("action SAVE");
			String textareaString = textArea.getText();
			Path path = getDefaultPath(Paths.get("").toAbsolutePath().toString() + "\\out");
			try {
				Files.createDirectories(path.getParent());
				Files.write(path, textareaString.getBytes());
			} catch (IOException e) {
				logError(e);
			}
		});

		MENU_ACTIONS.put(SAVE_AS, actionEvent -> {
			LOGGER.info("action SAVE_AS");
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setSelectedFile(getDefaultPath(null).toFile());
			if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				LOGGER.info("selected file: " + file.getName());
				try {
					String textareaString = textArea.getText();
					Path path = file.toPath();
					Files.write(path, textareaString.getBytes());
				} catch (IOException e) {
					logError(e);
				}
			}
		});
	}

	private void logError(Exception e) {
		LOGGER.severe(e.getMessage());
		LOGGER.severe(Arrays.stream(e.getStackTrace()).map(Object::toString).collect(Collectors.joining(System.lineSeparator())));
	}

	private Path getDefaultPath(String directoryName) {
		return Paths.get(directoryName, "function_parse_input_" + LocalDateTime.now().toString().replaceAll(":", "_") + ".txt");
	}

}
