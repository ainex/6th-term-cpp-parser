package edu.ulyanova.nstu.sixthterm.services;

import org.antlr.v4.gui.TreeViewer;

/**
 * Parsing service interface
 */
public interface ParseService {

	ParseResult parseToTree(String input);
}
