package edu.ulyanova.nstu.sixthterm.services;

import antlr4.FunctionDeclarationParser;
import org.antlr.v4.gui.TreeViewer;

/**
 * Result of the Function Declaration parsing.
 * Contains the {@link TreeViewer} and {@link DescriptiveErrorListener}
 */
public class ParseResult {
	private final TreeViewer treeViewer;
	private final FunctionDeclarationParser.FunctionDeclarationContext context;
	private final DescriptiveErrorListener errorListener;

	public ParseResult(FunctionDeclarationParser.FunctionDeclarationContext context, TreeViewer treeViewer, DescriptiveErrorListener errorListener) {
		this.treeViewer = treeViewer;
		this.context = context;
		this.errorListener = errorListener;
	}

	public ParseResult(FunctionDeclarationParser.FunctionDeclarationContext context, DescriptiveErrorListener errorListener) {
		this.context = context;
		this.treeViewer = null;
		this.errorListener = errorListener;
	}

	public TreeViewer getTreeViewer() {
		return treeViewer;
	}

	public FunctionDeclarationParser.FunctionDeclarationContext getContext() {
		return context;
	}

	public DescriptiveErrorListener getErrorListener() {
		return errorListener;
	}
}
