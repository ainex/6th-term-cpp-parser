package edu.ulyanova.nstu.sixthterm.services;

import antlr4.FunctionDeclarationLexer;
import antlr4.FunctionDeclarationParser;
import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.*;

import java.util.Arrays;


/**
 * Implementation of {@link ParseService}
 */
public class ParseServiceImpl implements ParseService {
	@Override
	public ParseResult parseToTree(String expression) {

		try {
			//lexer splits input into tokens
			CharStream input = new ANTLRInputStream(expression);
			FunctionDeclarationLexer functionDeclarationLexer = new FunctionDeclarationLexer(input);
			TokenStream tokens = new CommonTokenStream(functionDeclarationLexer);

			//parser generates abstract syntax tree
			FunctionDeclarationParser parser = new FunctionDeclarationParser(tokens);

			DescriptiveErrorListener errorListener = new DescriptiveErrorListener();
			functionDeclarationLexer.removeErrorListeners();
			functionDeclarationLexer.addErrorListener(errorListener);
			parser.removeErrorListeners();
			parser.addErrorListener(errorListener);

			FunctionDeclarationParser.FunctionDeclarationContext context = parser.functionDeclaration();
			TreeViewer treeViewer = new TreeViewer(Arrays.asList(parser.getRuleNames()), context);

			//create result
			ParseResult result = new ParseResult(context, treeViewer, errorListener);
			//treeViewer.setScale(1.5);//scale a little

			System.out.println(context.toStringTree());

			return result;
		} catch (RecognitionException e) {
			throw new IllegalStateException("Recognition exception is never thrown, only declared.");
		}
	}
}
