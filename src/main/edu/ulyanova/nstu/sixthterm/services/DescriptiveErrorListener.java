package edu.ulyanova.nstu.sixthterm.services;

import edu.ulyanova.nstu.sixthterm.MainForm;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 */
public class DescriptiveErrorListener extends BaseErrorListener {
	private static final Logger LOGGER = Logger.getLogger(MainForm.class.getName());

	//public static DescriptiveErrorListener INSTANCE = new DescriptiveErrorListener();
	private String errorMessages = null;

	@Override
	public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol,
	                        int line, int charPositionInLine,
	                        String msg, RecognitionException e) {

		String sourceName = recognizer.getInputStream().getSourceName();
		if (!sourceName.isEmpty()) {
			sourceName = String.format("%s:%d:%d: ", sourceName, line, charPositionInLine);
		}

		String errorMessage = "line " + line + ":" + charPositionInLine + " " + msg;
		LOGGER.log(Level.SEVERE, sourceName + errorMessage);
		errorMessages = (errorMessages == null ? errorMessage : errorMessages + System.lineSeparator() + errorMessage);
	}

	public String getErrorMessages() {
		return errorMessages;
	}
}