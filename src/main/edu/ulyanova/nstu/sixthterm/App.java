package edu.ulyanova.nstu.sixthterm;

import java.io.IOException;
import java.util.logging.LogManager;

/**
 * Entry point
 */
public class App {

	public static void main(String[] args) throws IOException {
		//set up logger properties
		LogManager.getLogManager().readConfiguration(App.class.getResourceAsStream("/logger.properties"));

		new MainForm().init();
	}
}